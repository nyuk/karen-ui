import React from 'react';

import 'src/style/app.scss';

import UserLoader from "src/components/Loaders/UserLoader/UserLoader";

function MyApp({Component, pageProps}) {
  return (
    <React.Fragment>
      <UserLoader/>
      <Component {...pageProps} />
    </React.Fragment>
  );
}

export default MyApp;
