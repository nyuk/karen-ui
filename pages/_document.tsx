import Document, {Html, Head, Main, NextScript} from "next/document";
import React from "react";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return {...initialProps}
  }

  render() {
    return (
      <Html>
        <Head>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Saira+Semi+Condensed:400,500,700&display=swap&subset=vietnamese"/>
          <script src={"https://www.google.com/recaptcha/api.js?render=" + process.env.RECAPTCHA_KEY}/>
        </Head>
        <body style={{backgroundColor: "#161b28"}}>
        <Main/>
        <NextScript/>
        </body>
      </Html>
    )
  }
}

export default MyDocument;
