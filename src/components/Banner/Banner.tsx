import React from "react";

declare interface IProps {
  title: string;
  subtitle?: string;
}

class Banner extends React.Component<IProps, any> {
  render(): React.ReactNode {
    return (
      <section className="component--banner">
        <div className="container">
          <div className="banner--wrapper">
            <h3>{this.props.title}</h3>
            <h6>{this.props.subtitle}</h6>
          </div>
        </div>
      </section>
    );
  }
}

export default Banner;
