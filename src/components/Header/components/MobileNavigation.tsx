import {faEllipsisH, faThLarge, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {IconProp} from "@fortawesome/fontawesome-svg-core";
import Link from "next/link";
import React from "react";

class MobileNavigation extends React.Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {
      isExpanded: false,
    };

    this.expandClassNames = this.expandClassNames.bind(this);
    this.onTriggerExpand = this.onTriggerExpand.bind(this);
    this.onTriggerCollapse = this.onTriggerCollapse.bind(this);
  }

  expandClassNames(classNames) {
    if (this.state.isExpanded) {
      classNames.push("expanded");
    }

    return classNames.join(" ");
  }

  onTriggerExpand() {
    this.setState({
      isExpanded: true,
    });
  }

  onTriggerCollapse() {
    this.setState({
      isExpanded: false,
    });
  }

  render(): React.ReactNode {
    return (
      <section className="mobile--navigation">
        <div className="navigation--trigger" onClick={this.onTriggerExpand}>
          <FontAwesomeIcon icon={faEllipsisH}/>
        </div>

        <div className={this.expandClassNames(["navigation--backdrop"])} onClick={this.onTriggerCollapse}/>

        <div className={this.expandClassNames(["navigation--content"])}>
          <div className="container">
            <div className="content--action">
              <div className="action--close" onClick={this.onTriggerCollapse}>
                <FontAwesomeIcon icon={faTimes} fixedWidth/>
              </div>
            </div>

            <div className="content--heading">
              <h6>Navigation</h6>
            </div>

            <ContentLink icon={faThLarge} href="/" title="Trang Chủ"/>
          </div>
        </div>
      </section>
    );
  }
}

export default MobileNavigation;

declare interface IContentLinkProps {
  icon: IconProp;
  href: string;
  title: string;
}

class ContentLink extends React.Component<IContentLinkProps, any> {
  render(): React.ReactNode {
    return (
      <div className="content--link">
        <div className="link--icon">
          <FontAwesomeIcon icon={this.props.icon} fixedWidth/>
        </div>

        <div className="link--title">
          <Link href={this.props.href}>
            <a>{this.props.title}</a>
          </Link>
        </div>
      </div>
    );
  }
}
