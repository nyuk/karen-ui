import {faPlus, faPowerOff} from "@fortawesome/free-solid-svg-icons";
import {faEnvelopeOpen as farEnvelopeOpen} from "@fortawesome/free-regular-svg-icons";
import {withRouter} from "next/router";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React from "react";

import HttpService from "src/services/HttpService";
import StateService from "src/services/StateService";

declare interface IProps {
  user: any;
  router: any;
}

class Profile extends React.Component<IProps, any> {
  constructor(props) {
    super(props);
    this.onClickCompose = this.onClickCompose.bind(this)
  }

  onClickCompose() {
    if (this.props.user === null) {
      this.props.router.push("/login");
    } else {
      this.props.router.push("/me/threads");
    }
  }

  async onLogout() {
    const {status} = await HttpService.get("/logout");
    if (status !== 200) {
      return;
    }

    StateService.updateState({
      user: null,
    });
  }

  render(): React.ReactNode {
    const isLogin = this.props.user !== null;

    return (
      <section className="header--profile">
        <div className="profile--actions">
          <div className="actions--item" onClick={this.onClickCompose}>
            <FontAwesomeIcon icon={faPlus}/>
          </div>

          <div className="actions--item">
            <FontAwesomeIcon icon={farEnvelopeOpen}/>
          </div>

          {isLogin && (
            <div className="actions--item" onClick={this.onLogout}>
              <FontAwesomeIcon icon={faPowerOff}/>
            </div>
          )}
        </div>

        {!isLogin && (
          <div className="profile--login">
            <Link href={"/login"}>
              <a className="btn btn-primary">
                Đăng Nhập
              </a>
            </Link>
          </div>
        )}
      </section>
    );
  }
}

export default withRouter(Profile);
