import _ from "lodash";
import React from "react";

import CompactSearch from "./CompactSearch";
import LargeSearch from "./LargeSearch";
import HttpService from "src/services/HttpService";

declare interface IState {
  data: any,
  keyword: string,
  isExpanded: boolean,
  isLoading: boolean,
}

class Search extends React.Component<any, IState> {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      keyword: "",
      isExpanded: false,
      isLoading: false,
    };

    this.onSearch = this.onSearch.bind(this);
    this.onKeywordChange = this.onKeywordChange.bind(this);
    this.onKeywordDown = this.onKeywordDown.bind(this);
    this.onTriggerCollapse = this.onTriggerCollapse.bind(this);
  }

  async onSearch() {
    const keyword = this.state.keyword;
    if (keyword.length < 3) {
      return;
    }

    this.setState({
      data: [],
      isExpanded: false,
      isLoading: true,
    });

    const {data} = await HttpService.post("/search", {keyword}, {withReCAPTCHA: true});
    if (data !== null) {
      this.setState({
        data: _.get(data, 'data', []),
      });
    }

    this.setState({
      isExpanded: true,
      isLoading: false,
    });
  }

  onKeywordChange(e) {
    this.setState({
      keyword: e.target.value,
    });
  }

  onKeywordDown(e) {
    if (e.keyCode === 13) {
      this.onSearch();
    }
  }

  onTriggerCollapse() {
    this.setState({
      data: [],
      keyword: "",
      isExpanded: false,
    });
  }

  render(): React.ReactNode {
    return (
      <React.Fragment>
        <LargeSearch
          data={this.state.data}
          keyword={this.state.keyword}
          isExpanded={this.state.isExpanded}
          isLoading={this.state.isLoading}
          onSearch={this.onSearch}
          onKeywordChange={this.onKeywordChange}
          onKeywordDown={this.onKeywordDown}
          onTriggerCollapse={this.onTriggerCollapse}/>

        <CompactSearch
          data={this.state.data}
          keyword={this.state.keyword}
          isExpanded={this.state.isExpanded}
          isLoading={this.state.isLoading}
          onSearch={this.onSearch}
          onKeywordChange={this.onKeywordChange}
          onKeywordDown={this.onKeywordDown}
          onTriggerCollapse={this.onTriggerCollapse}/>
      </React.Fragment>
    );
  }
}

export default Search;
