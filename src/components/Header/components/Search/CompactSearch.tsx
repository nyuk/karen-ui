import {faSearch, faSpinner, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import React from "react";
import SearchSection from "./SearchSection";

declare interface IProps {
  data: any;
  keyword: string;
  isExpanded: boolean;
  isLoading: boolean;
  onSearch: any;
  onKeywordChange: any;
  onKeywordDown: any;
  onTriggerCollapse: any;
}

class CompactSearch extends React.Component<IProps, any> {
  expandClassNames(classNames) {
    if (this.props.isExpanded) {
      classNames.push("expanded");
    }

    return classNames.join(" ");
  }

  render(): React.ReactNode {
    const props = this.props;

    let searchButtonAction = props.onSearch;
    let searchButtonIcon = props.isLoading ? faSpinner : faSearch;
    if (props.isExpanded) {
      searchButtonAction = props.onTriggerCollapse;
      searchButtonIcon = faTimes;
    }

    return (
      <section className="compact--search">
        <div className="search--input">
          <input type="text" placeholder="Nhập để tìm kiếm..." value={props.keyword}
                 onChange={props.onKeywordChange}
                 onKeyDown={props.onKeywordDown}/>
        </div>

        <div className="search--button" onClick={searchButtonAction}>
          <FontAwesomeIcon icon={searchButtonIcon} spin={props.isLoading} fixedWidth/>
        </div>

        <div className={this.expandClassNames(["search--backdrop"])}/>

        <div className={this.expandClassNames(["search--content"])}>
          <div className="container">
            {this.props.data.map(section => <SearchSection key={section.section} section={section}/>)}
          </div>
        </div>
      </section>
    );
  }
}

export default CompactSearch;
