import React from "react";
import Link from "next/link";

declare interface IProps {
  section: ISection;
}

declare interface ISection {
  section: string;
  title: string;
  records: any;
}

class SearchSection extends React.Component<IProps, any> {
  render(): React.ReactNode {
    const section = this.props.section;

    return (
      <section key={section.section} className="search--section">
        <div className="section--heading">
          <h6>{section.title}</h6>
        </div>

        {section.records.map((item) => {
          const itemURL = section.section + "/details?slug=" + item.slug;

          return (
            <div key={item.id} className="section--item">
              <div className="item--thumbnail">
                <img alt="" src={item.thumbnail} width={48} height={48}/>
              </div>

              <div className="item--info">
                <div className="info--title">
                  <h6>
                    <Link href={itemURL}>
                      <a>{item.title}</a>
                    </Link>
                  </h6>
                </div>

                <p className="info--subtitle">
                  {item.subtitle}
                </p>
              </div>
            </div>
          );
        })}
      </section>
    );
  }
}

export default SearchSection;
