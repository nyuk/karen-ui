import Link from "next/link";
import React from "react";

import {withUser} from "src/libs/withUser";

import MobileNavigation from "./components/MobileNavigation";
import Profile from "./components/Profile/Profile";
import Search from "./components/Search/Search";

declare interface IProps {
  user?: any | null;
}

class Header extends React.Component<IProps, any> {
  render(): React.ReactNode {
    const {user} = this.props;

    return (
      <section className="header">
        <div className="header--brand">
          <Link href={"/"}>
            <a><img alt={"Logo"} src={"/assets/images/logo.svg"} width={40} height={40}/></a>
          </Link>
        </div>

        <MobileNavigation/>

        <Search/>

        <Profile user={user}/>
      </section>
    );
  }
}

export default withUser(Header);
