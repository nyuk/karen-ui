import _ from "lodash";
import React from "react";

import HttpService from "src/services/HttpService";
import StateService from "src/services/StateService";

class UserLoader extends React.Component<any, any> {
  componentDidMount(): void {
    this.retrieveAuthStatus().catch(console.error);
  }

  async retrieveAuthStatus() {
    const {data, status} = await HttpService.get('/users/me');
    if (status !== 200) {
      return;
    }

    StateService.updateState({
      user: _.get(data, "data"),
    });
  }

  render(): React.ReactNode {
    return <React.Fragment/>
  }
}

export default UserLoader;
