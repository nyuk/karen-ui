import React from "react";
import PubSub from "pubsub-js";

import StateService, {STATE_UPDATED} from "src/services/StateService";

export function withUser(Component: any) {
  // @ts-ignore
  return class extends React.Component<any, any> {
    pubsubToken: string;

    constructor(props) {
      super(props);

      this.state = {
        user: StateService.getState("user", null),
      };
    }

    componentDidMount(): void {
      const self = this;

      this.pubsubToken = PubSub.subscribe(STATE_UPDATED, function () {
        self.setState({
          user: StateService.getState("user", null),
        });
      });
    }

    componentWillUnmount(): void {
      PubSub.unsubscribe(this.pubsubToken);
    }

    render(): React.ReactNode {
      // @ts-ignore
      return <Component user={this.state.user} {...this.props}/>;
    }
  }
}
