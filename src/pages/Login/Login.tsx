import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import {withRouter} from "next/router";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Head from "next/head";
import Link from "next/link";
import React from "react";

import HttpService from "src/services/HttpService";
import StateService from "src/services/StateService";

declare interface IState {
  email: string;
  password: string;
  isError: boolean;
  isLoading: boolean;
}

class Login extends React.Component<any, IState> {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      isError: false,
      isLoading: false,
    };

    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  navigateBack() {
    // TODO: Use the RouteLoader to record & navigate
    //  back to the latest URL.
    this.props.router.push("/").catch(console.error);
  }

  onEmailChange(e) {
    this.setState({
      email: e.target.value,
    });
  }

  onPasswordChange(e) {
    this.setState({
      password: e.target.value,
    });
  }

  async onSubmit(e) {
    e.preventDefault();

    this.setState({
      isError: false,
      isLoading: true,
    })

    await HttpService.get("/sanctum/csrf-cookie");

    let {email, password} = this.state;
    let {data, status} = await HttpService.post("/login", {email, password}, {withReCAPTCHA: true});

    if (status !== 200) {
      this.setState({
        isError: true,
        isLoading: false,
      })

      return;
    }

    StateService.updateState({
      "user": data.data,
    });

    this.navigateBack();
  }

  render(): React.ReactNode {
    const {email, password, isError, isLoading} = this.state;

    return (
      <section id="login">
        <Head>
          <title>Đăng Nhập - Nyuk Project</title>
        </Head>

        <div className="container login--content">
          <form onSubmit={this.onSubmit}>
            <div className="content--title">
              <h4 className="text-center mb-0">Đăng Nhập</h4>
            </div>

            <div className="content--error" style={{display: isError ? "block" : "none"}}>
              <p>Đăng nhập không thành công, vui lòng thử lại!</p>
            </div>

            <div className="content--form">
              <div className="form-group">
                <input type="email" className="form-control" placeholder="Username hoặc Email" value={email}
                       onChange={this.onEmailChange} required/>
              </div>

              <div className="form-group">
                <input type="password" className="form-control" placeholder="Mật khẩu" value={password}
                       onChange={this.onPasswordChange} minLength={8} required/>
              </div>

              <div className="form-group small text-muted text-right">
                <Link href={"/reset-password"}>
                  <a>Quên Mật Khẩu?</a>
                </Link>
              </div>

              <div className="form-group mb-0">
                <button type="submit" className="btn btn-primary btn-lg btn-block" disabled={isLoading}>
                  {isLoading ? <FontAwesomeIcon icon={faSpinner} fixedWidth spin/> : "Đăng Nhập"}
                </button>
              </div>
            </div>
          </form>
        </div>
      </section>
    )
  }
}

export default withRouter(Login);
