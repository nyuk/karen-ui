import Link from "next/link";
import Head from "next/head";
import React from "react";

import CommonService from "src/services/CommonService";

class Error extends React.Component<any, any> {
  render(): React.ReactNode {
    return (
      <section id="error--404">
        <Head>
          <title>{CommonService.generateTitle("Lỗi 404")}</title>
        </Head>

        <div className="card error--content">
          <div className="card-body">
            <h1 className="content--title">
              Lỗi 404
            </h1>

            <p className="content--description">
              Nội dung bạn đang truy cập không tồn tại, vì một lý do thần kì nào đó... Nhưng đừng lo, bạn luôn có thể
              quay trở lại trang chủ mà :D
            </p>

            <p className="content--action">
              <Link href="/">
                <a className="btn btn-success btn-action">
                  Trang Chủ
                </a>
              </Link>
            </p>
          </div>
        </div>
      </section>
    );
  }
}

export default Error;
