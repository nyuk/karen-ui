import Link from "next/link";
import Head from "next/head";
import React from "react";

import CommonService from "src/services/CommonService";

class Error extends React.Component<any, any> {
  render(): React.ReactNode {
    return (
      <section id="error--401">
        <Head>
          <title>{CommonService.generateTitle("Lỗi 401")}</title>
        </Head>

        <div className="card error--content">
          <div className="card-body">
            <h1 className="content--title">
              Lỗi 401
            </h1>

            <p className="content--description">
              Bạn cần đăng nhập để có thể xem được nội dung này, hãy nhấn nút bên dưới để tiếp tục!
            </p>

            <p className="content--action">
              <Link href="/login">
                <a className="btn btn-success btn-action">
                  Đăng Nhập
                </a>
              </Link>
            </p>
          </div>
        </div>
      </section>
    );
  }
}

export default Error;
