import React from "react";

import Header from "src/components/Header/Header";

class Home extends React.Component<any, any> {
  render(): React.ReactNode {
    return (
      <section id="Home">
        <Header/>

        <div className="container mt-5">
          <p className="text-muted text-center">
            Hệ thống đang được bảo trì, vui lòng quay lại sau!
          </p>
        </div>
      </section>
    );
  }
}

export default Home;
