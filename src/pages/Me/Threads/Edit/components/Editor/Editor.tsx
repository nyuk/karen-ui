import React from "react";

declare interface IProps {
  onChange: (any) => void;
}

class Editor extends React.Component<IProps, any> {
  editor: any;

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount(): void {
    const EditorJS = require("@editorjs/editorjs");
    const Header = require("@editorjs/header");
    const List = require("@editorjs/list");

    this.editor = new EditorJS({
      holder: "editorjs",
      tools: {
        header: Header,
        list: List,
      },
      placeholder: "Nội dung bài viết",
      onChange: this.onChange,
    });
  }

  onChange() {
    const self = this;

    this.editor.save()
      .then(self.props.onChange)
      .catch(console.error);
  }

  render(): React.ReactNode {
    return (
      <div id="editorjs"/>
    );
  }
}

export default Editor;
