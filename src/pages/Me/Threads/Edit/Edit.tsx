import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import _ from "lodash";
import React from "react";

import {withUser} from "src/libs/withUser";
import Header from "src/components/Header/Header";
import HttpService from "src/services/HttpService";
import Error from "src/pages/Error/401";

import Editor from "./components/Editor/Editor";

declare interface IProps {
  user?: any | null;
}

declare interface IState {
  title: string;
  content: any;
  isLoading: boolean;
}

class Edit extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      content: {},
      isLoading: false,
    };

    this.onContentChange = this.onContentChange.bind(this);
    this.onTitleChange = this.onTitleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.toggleLoading = this.toggleLoading.bind(this);
  }

  onContentChange(data) {
    this.setState({
      content: data,
    });
  }

  onTitleChange(e) {
    this.setState({
      title: e.target.value,
    });
  }

  async onSubmit() {
    let title = this.state.title.trim();
    let content = this.state.content;

    const validateResult = validateInput({
      title: title,
      content: content,
    });
    if (validateResult.error !== null) {
      alert(validateResult.error);
      return;
    }

    this.toggleLoading(true);

    const {data, status} = await HttpService.post("/threads", {title, content}, {withReCAPTCHA: true});
    if (status !== 200) {
      this.toggleLoading(false);

      alert("Đã có lỗi xảy ra, vui lòng thử lại!");
      alert(JSON.stringify(data, null, 2));
      return;
    }

    console.log(data, status);
  }

  toggleLoading(isLoading: boolean) {
    this.setState({
      isLoading,
    });
  }

  render(): React.ReactNode {
    if (this.props.user === null) {
      return <Error/>;
    }

    const {title, isLoading} = this.state;

    return (
      <section id="me--threads--edit">
        <Header/>

        <div className="container mt-5">
          <div className="row">
            <div className="col-8">
              <div className="card mb-4">
                <div className="card-body px-4 py-2">
                  <div className="form-group m-0">
                    <input type="text" placeholder="Tiêu đề bài biết" className="form-control border-0 py-4"
                           value={title} onChange={this.onTitleChange}/>
                  </div>

                </div>
              </div>

              <div className="card">
                <div className="card-body px-0 py-4">
                  <Editor onChange={this.onContentChange}/>
                </div>
              </div>
            </div>

            <div className="col-4">
              <div className="card">
                <div className="card-body">
                  <button className="bt btn-block btn-primary btn-action" onClick={this.onSubmit} disabled={isLoading}>
                    {isLoading ? <FontAwesomeIcon icon={faSpinner} spin fixedWidth/> : "Lưu Bài Viết"}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withUser(Edit);

declare interface IValidateInputRequest {
  title: string;
  content: any;
}

declare interface IValidateInputResponse {
  error: string | null;
}

function validateInput(input: IValidateInputRequest): IValidateInputResponse {
  if (input.title.length < 1) {
    return {
      error: "Tiêu đề bài viết không được để trống!",
    };
  }

  if (_.get(input.content, "blocks.length", 0) < 1) {
    return {
      error: "Nội dung bài viết không được để trống!"
    };
  }

  return {
    error: null,
  };
}
