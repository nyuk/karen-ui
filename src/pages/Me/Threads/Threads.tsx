import {faPlus, faCog} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React from "react";

import {withUser} from "src/libs/withUser";
import Banner from "src/components/Banner/Banner";
import Header from "src/components/Header/Header";
import HttpService from "src/services/HttpService";
import Error from "src/pages/Error/401";

declare interface IProps {
  user?: any | null;
}

declare interface IState {
  threads: any;
}

class Threads extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      threads: [],
    };

    this.retrieveThreads = this.retrieveThreads.bind(this);
  }

  componentDidMount(): void {
    if (this.props.user === null) {
      return;
    }

    this.retrieveThreads().catch(console.error);
  }

  componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any): void {
    if (this.props.user === prevProps.user || this.props.user === null) {
      return;
    }

    this.retrieveThreads().catch(console.error);
  }

  async retrieveThreads() {
    const {data, status} = await HttpService.get("/users/me/threads");
    if (status !== 200) {
      alert("Đã có lỗi xảy ra, vui lòng thử lại!");
      return;
    }

    this.setState({
      threads: data.data,
    });
  }

  render(): React.ReactNode {
    if (this.props.user === null) {
      return <Error/>
    }

    return (
      <section id="me--threads">
        <Header/>

        <div className="mb-5 mt-5">
          <Banner title="Profile Hub" subtitle="Tài khoản, nội dung, tin nhắn và nhiều thứ nữa!"/>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <div className="card">
                <div className="list-group list-group-striped list-group-flush">
                  <Link href={"/me/threads"}>
                    <a className="list-group-item list-group-item-action">
                      Bài viết Của bạn
                    </a>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-md-9">
              <div className="text-right mb-4">
                <Link href={"/me/threads/edit"}>
                  <a className="btn btn-primary btn-action">
                    <FontAwesomeIcon icon={faPlus}/>
                    <span>Bài Viết Mới</span>
                  </a>
                </Link>
              </div>

              {this.state.threads.map(thread => <ThreadItem
                key={thread.id}
                id={thread.id}
                title={thread.title}
                status={thread.status}
                created_at={thread.created_at}
              />)}
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withUser(Threads);

declare interface IThreadItemProps {
  id: number;
  title: string;
  status: string;
  created_at: string;
}

class ThreadItem extends React.Component<IThreadItemProps, any> {
  render(): React.ReactNode {
    const props = this.props;

    const readUrl = "/threads/details?id=" + props.id;
    const editUrl = "/me/threads/edit?id=" + props.id;

    const statusClassNames = ["btn", "text-uppercase", "mr-2"];
    switch (props.status) {
      case "pending":
        statusClassNames.push("btn-outline-secondary");
        break;
      default:
        statusClassNames.push("btn-outline-secondary");
    }

    return (
      <div className="card card--threads">
        <div className="card-body">
          <div className="threads--info">
            <p className="info--title">
              <Link href={readUrl}>
                <a>{props.title}</a>
              </Link>
            </p>

            <p className="info--subtitle">
              {props.created_at}
            </p>
          </div>

          <div className="threads-action">
            <button className={statusClassNames.join(" ")} disabled>
              {props.status}
            </button>

            <Link href={editUrl}>
              <a className="btn btn-outline-secondary">
                <FontAwesomeIcon icon={faCog} fixedWidth/>
              </a>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
