import _ from "lodash";
import PubSub from "pubsub-js";

export const STATE_UPDATED = "STATE_UPDATED";

class StateService {
  static getState(path = "", defaultValue = undefined) {
    if (typeof window === "undefined") {
      return defaultValue;
    }

    return _.get(window, "state." + path, defaultValue)
  }

  static updateState(payload = {}) {
    if (typeof window === "undefined") {
      return;
    }

    if (typeof window["state"] !== "object") {
      window["state"] = {};
    }

    window["state"] = Object.assign(window["state"], payload);

    PubSub.publish(STATE_UPDATED, null);
  }
}

export default StateService;
