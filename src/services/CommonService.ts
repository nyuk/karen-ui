class CommonService {
  static generateTitle(title: string) {
    return title + " — Nyuk Project";
  }
}

export default CommonService;
